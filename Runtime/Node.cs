﻿using System.Collections;
using UnityEngine;

namespace DigitomNodes
{
    public abstract class Node : MonoBehaviour, INode
    {
        public virtual INode InConnection { get; protected set; }
        public virtual INode OutConnection { get; protected set; }

        protected abstract bool IsInConnectionValid(INode inConnection);
        protected abstract bool IsOutConnectionValid(INode outConnection);
        protected abstract bool IsInDisconnectionValid(INode inConnection);
        protected abstract bool IsOutDisconnectionValid(INode outConnection);

        public virtual bool TryInDisconnect(INode inConnection)
        {
            if (!IsInDisconnectionValid(inConnection)) return false;
            inConnection.ConfirmOutDisconnect(this);
            InConnection = null;
            return true;
        }

        public virtual bool TryOutDisconnect(INode outConnection)
        {
            if (!IsOutDisconnectionValid(outConnection)) return false;
            outConnection.ConfirmInDisconnect(this);
            OutConnection = null;
            return true;
        }

        public virtual bool TryInConnect(INode inConnection)
        {
            if (!IsInConnectionValid(inConnection)) return false;
            inConnection.ConfirmOutConnect(this);
            InConnection = inConnection;
            return true;
        }

        public virtual bool TryOutConnect(INode outConnection)
        {
            if (!IsOutConnectionValid(outConnection)) return false;
            outConnection.ConfirmInConnect(this);
            OutConnection = outConnection;
            return true;
        }

        public virtual void ConfirmInConnect(INode inConnection)
        {
            InConnection = inConnection;
        }

        public virtual void ConfirmOutConnect(INode outConnection)
        {
            OutConnection = outConnection;
        }

        public virtual void ConfirmInDisconnect(INode inConnection)
        {
            InConnection = null;
        }

        public virtual void ConfirmOutDisconnect(INode outConnection)
        {
            OutConnection = null;
        }

        public virtual INode Root(int maxNodeConnections = 1000)
        {
            int count = 0;
            INode curNode = this;
            while (curNode.InConnection != null && count < maxNodeConnections)
            {
                curNode = curNode.InConnection;
                if (curNode.Equals(this)) break; //loop. break.
                count++;
                if (count >= maxNodeConnections)
                    Debug.LogError($"{this} has reached max count searching for it's root node. This is a loop error OR need to increase max connections.");
            }
            return curNode;
        }
    }
}