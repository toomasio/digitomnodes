﻿using System.Collections;
using UnityEngine;

namespace DigitomNodes
{
    public class NodeDebug : Node
    {
        [SerializeField] private Node inConnection = null;
        [SerializeField] private Node outConnection = null;
        [SerializeField] private bool debugRoot = false;

        public override INode InConnection { get => inConnection; protected set => inConnection = (Node)value; }
        public override INode OutConnection { get => outConnection; protected set => outConnection = (Node)value; }
        protected override bool IsInConnectionValid(INode inConnection) => true;
        protected override bool IsOutConnectionValid(INode outConnection) => true;
        protected override bool IsInDisconnectionValid(INode inConnection) => true;
        protected override bool IsOutDisconnectionValid(INode outConnection) => true;

        private Node lastInConnection;
        private Node lastOutConnection;

        private void Start()
        {
            if (debugRoot)
                Debug.Log($"{Root()} ");
        }

        private void OnValidate()
        {
            if (outConnection != null)
            {
                outConnection.TryInConnect(this);
                lastOutConnection = outConnection;
            }  
            else if (lastOutConnection != null)
            {
                lastOutConnection.TryInDisconnect(this);
                lastOutConnection = null;
            }
                
            if (inConnection != null)
            {
                inConnection.TryOutConnect(this);
                lastInConnection = inConnection;
            }    
            else if (lastInConnection != null)
            {
                lastInConnection.TryOutDisconnect(this);
                lastInConnection = null;
            }
                
        }
    }
}