using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomNodes
{
    public interface INodeConnection
    {
        INode ConnectedTo { get; }
    }
}
