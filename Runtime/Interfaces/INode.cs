using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomNodes
{
    public interface INode
    {
        INode InConnection { get; }
        INode OutConnection { get; }
        bool TryInConnect(INode inConnection);
        bool TryOutConnect(INode outConnection);
        bool TryInDisconnect(INode inConnection);
        bool TryOutDisconnect(INode outConnection);
        void ConfirmInConnect(INode inConnection);
        void ConfirmOutConnect(INode outConnection);
        void ConfirmInDisconnect(INode inConnection);
        void ConfirmOutDisconnect(INode outConnection);
    }
}
